
const router = require("express").Router();
const parking = require('../controller/parking.controller');
const park = new parking();

// creating routes for All the functions defined in parking.controller file
router.post('/createParkingLot', park.createParkingLot);

router.post('/carPark', park.carPark);

router.get('/parkCarInfo/:spaceId', park.getParkCarInfo);

router.post('/unparkCar', park.unparkCar);

router.get('/', park.getAllParking);

module.exports = router