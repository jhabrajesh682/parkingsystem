
const parking = require('../models/parking.model');
const validator = require('../validator/parking.validator');


/**
 * @description a base class for Parking System
 * @author Brajesh Jha <jhabrajesh682@gmail.com>
 */

class park {


	/**
	 * @param {parkingLotSize, parkingName, parkingSpace, spaceOccupied} input user's input via req.body
	 * @Info {parkingSpace} you can pass empty array
	 * @description creates a parking lot with maximum slot numbers.
	 * It throws an error if the above input will not passed
	 */
	async createParkingLot(req, res) {

		// validating the user's input.
		let { error } = validator.validateCreateParkingLot(req.body);
		if (error) {
			return res.status(400).send({
				status: false,
				message: 'something went wrong',
				error: error
			})
		}

		const createParking = new parking({
			...req.body
		})
		// if parkingLotSize is  not passed in input then it will take lot size from .env file
		createParking.parkingLotSize = req.body.parkingLotSize || process.env.PARKINGLOTCAPACITY
		await createParking.save();

		return res.status(200).send({
			status: true,
			message: 'parking lot successfully created!',
			result: createParking
		})
	}


	/**
	 * @param {carNumber, parkingId} input user's input via req.body
	 * @description you can park your car in the provided parking Id
	 * @info you can get parkingId from this API 'http://localhost:3030/api/v1/park/'
	 * It throws an error if the above input will not passed
	 */

	async carPark(req, res) {

		let { error } = validator.validateCarPark(req.body);
		if (error) {
			return res.status(400).send({
				status: false,
				error: error
			})
		}
		const parkInfo = await parking.findById(req.body.parkingId).lean();
		if (!parkInfo) {
			return res.status(400).send({
				status: false,
				message: 'Car parking not found'
			})
		}
		// checking for the parking info. Space is available or not.
		if (parkInfo.spaceOccupied >= parkInfo.parkingLotSize) {
			return res.status(400).send({
				status: false,
				message: `Sorry, parking lot is full`
			})
		}
		// generating 5 digit random number as spaceId for parked car
		const spaceId = Math.floor(Math.random() * 90000) + 10000;

		// updating all the details of car and also increasing spaceOccupied field in mongoDB
		await parking.updateOne({ _id: req.body.parkingId }, {
			$inc: { spaceOccupied: 1 }, $push: {
				parkingSpace: { carNumber: req.body.carNumber, spaceId: spaceId }
			}
		})
		return res.status(200).send({
			status: true,
			message: 'Your car successfully parked',
			ParkingNumber: spaceId
		})
	}


	/**
	 * @param {spaceId } input user's input via req.params
	 * @description you can get your parking car details
	 * It throws an error if the above input will not passed.
	 */

	async getParkCarInfo(req,res) {

		// validating user's input
		let {error} = validator.validateCarParkInfo(req.params)
		if (error) {
			return res.status(400).send({
				status: false,
				error: error
			})
		}

		//fetching the car parked details from parkingLot collections
		const parkingInfo = await parking.aggregate([
		{ $unwind: '$parkingSpace' },

		{ $match: { 'parkingSpace.spaceId': req.params.spaceId } },

		{ $project: { _id: 1, 'parkingSpace.carNumber': 1, 'parkingSpace.spaceId': 1 } }])

		return res.status(200).send({
			status: true,
			result: parkingInfo
		})
	}

	/**
	 * @param {spaceId, parkingId } input user's input via req.body
	 * @description you can unpark you car using this functions
	 * It throws an error if the above input will not passed.
	 */

	async unparkCar(req,res) {

		let {error} = validator.validateUnparkCar(req.body)
		if (error) {
			return res.status(400).send({
				status: false,
				error: error
			})
		}
		// removing the parked car from parkingSpace Array and decreasing the spaceOccupied by 1
		await parking.updateOne({ _id: req.body.parkingId }, {
			$inc: { spaceOccupied: -1 }, $pull: {
				parkingSpace: { spaceId: req.body.spaceId }
			}
		})

		return res.status(200).send({
			status: true,
			message: 'You car successfully unparked'
		})
	}

	/**
	 * @description it gives the details of all parking lot created by users
	 * and also provide parkingId as _id
	 */

	async getAllParking(req,res) {

		const parkings = await parking.find({}).sort({_id: -1}).lean();

		// it will return an array with all the parking lot details
		return res.status(200).send({
			status: true,
			result: parkings
		})
	}
}

module.exports = park