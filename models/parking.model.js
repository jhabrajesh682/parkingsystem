const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const parkSchema = new Schema({
  
    parkingLotSize: {
        type: Number,
        required: true
    },
    parkingName: {
        type: String,
        required: true
    },
    parkingSpace: [{
        carNumber: {
            type: String
        },
        spaceId: {
            type: String
        }
    }],
    spaceOccupied: {
        type: Number,
        default: 0
    }
},
{
  timestamps: true
});

module.exports = mongoose.model('parkingLot', parkSchema);