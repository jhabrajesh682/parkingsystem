# ParkingSystem



## Getting started

Step: 1
Run npm install

Step: 2
Run npm start

## Modules - MVC Approach


1. Models : it has one file `parking.model.js` it contain all the field which is created in DB.
2. Controller: it has file `parking.controller.js` 
#There are one class defined:

`park()`: It is the main class which is used to initialize a parking lot. it has functions like:

1. `createParkingLot()`: creates a parking lot with maximum slot numbers.
2. `carPark()`: this function is used to park your car in the provided parking Id.
3. `getParkCarInfo()`: you can get your parking car details. By providing spaceId.
4. `unparkCar()`: this functions is used to unpark you car.
5. `getAllParking()`: it gives the details of all parking lot created by users and also provide parkingId as _id

3. Routes: it contain all the routes of above functions


## POSTMAN COLLECTIONS DOCUMENT LINK:-
https://documenter.getpostman.com/view/16414694/Uz5GpGxY