require("dotenv").config();
const db = require("./utils/db");
const chalk = require("chalk");
const info = chalk.bold.blue
const express = require("express");
const bodyParser = require('body-parser')
const app = express();
app.use(bodyParser.json())


const park = require('./routers/parking.routes')
app.use('/api/v1/park', park)

console.log(new Date())

const port = process.env.PORT;
let serverlisten = app.listen(port, () => {
  db();
  console.log(info("Server started on port " + port));

});
