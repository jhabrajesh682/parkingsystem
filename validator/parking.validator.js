const Joi = require('joi');

// used by createParkingLot() in parking.controller.js file
function validateCreateParkingLot(park) {
    let schema = Joi.object({
        parkingLotSize: Joi.number(),
        parkingName: Joi.string().required(),
        parkingSpace: Joi.array(),
        spaceOccupied: Joi.number().required()
    })

    let result = schema.validate(park)
    return result
}

// used by carPark() in parking.controller.js file
function validateCarPark(park) {
    let schema = Joi.object({
        carNumber: Joi.string().required(),
        parkingId: Joi.string().required()
    })

    let result = schema.validate(park)
    return result
}

// used by getParkCarInfo() in parking.controller.js file
function validateCarParkInfo(park) {
    let schema = Joi.object({
        spaceId: Joi.string().required()
    })

    let result = schema.validate(park)
    return result
}

// used by unparkCar() in parking.controller.js file
function validateUnparkCar(park) {
    let schema = Joi.object({
        spaceId: Joi.string().required(),
        parkingId: Joi.string().required()
    })

    let result = schema.validate(park)
    return result
}

module.exports.validateCreateParkingLot = validateCreateParkingLot
module.exports.validateCarPark = validateCarPark
module.exports.validateCarParkInfo = validateCarParkInfo
module.exports.validateUnparkCar = validateUnparkCar